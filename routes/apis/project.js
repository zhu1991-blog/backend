const router = require('express').Router();

const Project = require('../../models/Project');
const userCheck = require('../../middleware/userCheck');
const ccCheck = require('../../middleware/ccCheck');

router.get('/:userId/prac', async (req, res) => {
    try {
        const { userId } = req.params;
        const projects = await Project.find({
            type: 'prac',
            _userId: userId
        }).sort({endTime: 'desc'});
        if (!projects) throw new Error(`Projects not found.`);
        res.send([...projects]);        
    } catch (e) {
        res.status(404).send({error: e});
    }
});

router.get('/:userId/proj', ccCheck, async (req, res) => {
    try {
        const { userId } = req.params;
        const projects = await Project.find({
            type: 'proj',
            _userId: userId
        }).sort({endTime: 'desc'});
        if (!projects) throw new Error(`Project not found.`);
        res.send([...projects]);          
    } catch (e) {
        res.status(404).send({error: e});
    }
});


router.get('/:userId/:projectId', async (req, res) => {
    try {
        const { projectId, userId } = req.params;
        const project = await Project.findOne({
            _id: projectId,
            _userId: userId
        });
        if (!project) throw new Error(`Project not found.(${projectId})`);
        res.send(project);        
    } catch (e) {
        res.status(404).send({error: e});
    }
});

router.get('/:userId', userCheck, async (req, res) => {
    try {
        const { userId } = req.params;
        if (userId !== req.user._id.toHexString()) {
            return res.status(401).send({error: 'User not correct.'});
        }
        let projects = await Project.find({
            _userId: userId
        });
        res.send([...projects].reverse());
    } catch(e) {
        res.status(400).send({error: e});
    }
});

router.post('/:userId', userCheck, async (req, res) => {
    try {
        const { userId } = req.params;
        if (userId !== req.user._id.toHexString()) {
            return res.status(401).send({error: 'User not correct.'});
        }
        const { name, startTime, endTime, langs, tools, os, desc, type, link, mainLang } = req.body;
        const project = await new Project({
            name, langs, tools, os, desc, type, link, mainLang,
            startTime: new Date(startTime),
            endTime: new Date(endTime),
            _userId: userId
        }).save();
        res.send([project]);
    } catch (e) {
        res.status(404).send({error: e});
    }
});

router.put('/:userId/:projectId', userCheck, async (req, res) => {
    try {
        const { projectId, userId } = req.params;
        if (userId !== req.user._id.toHexString()) {
            return res.status(401).send({error: 'User not correct.'});
        }
        const { name, startTime, endTime, langs, tools, os, desc, type, link, mainLang } = req.body;
        const project = await Project.findByIdAndUpdate(projectId, {
            name, langs, tools, os, desc, type, link, mainLang,
            startTime: new Date(startTime), 
            endTime: new Date(endTime),
            _userId: userId
        }, {
            new: true
        });
        if (!project) throw new Error(`Project not found.(${projectId})`);
        res.send(project);
    } catch (e) {
        res.status(404).send({error: e});
    }
});

router.delete('/:userId/:projectId', userCheck, async (req, res) => {
    try {
        const { projectId, userId } = req.params;
        if (userId !== req.user._id.toHexString()) {
            return res.status(401).send({error: 'User not correct.'});
        }
        const project = await Project.findOneAndRemove({_id: projectId, _userId: userId});
        if (!project) throw new Error(`Project not found.(${projectId})`);
        res.send(project);
    } catch (e) {
        res.status(404).send({error: e});
    }
});

module.exports = router;