const router = require('express').Router();

const User = require('../../models/User');
const userCheck = require('../../middleware/userCheck');
const ccCheck = require('../../middleware/ccCheck');

router.get('/:username/simple', async (req, res) => {
    try {
        const { username } = req.params;
        const user = await User.findOne({ username });
        if (!user) {
            throw new Error('User not found.');
        }
        res.send({
            _id: user._id,
            name: user.name,
            username: user.username,
            simpleProfile: user.simpleProfile,
            skills: user.skills
        });
    } catch (e) {
        res.status(404).send({ error: e });
    }
});

router.get('/:username', ccCheck, async (req, res) => {
    try {
        const { username } = req.params;
        const user = await User.findOne({ username });
        if (!user) {
            throw new Error('User not found.');
        }
        res.send({
            _id: user._id, username: user.username, name: user.name,
            fullName: user.fullName, profile: user.profile, 
            skills: user.skills, simpleProfile: user.simpleProfile,
            profileImg: user.profileImg
        });
    } catch (e) {
        res.status(404).send({ error: e });
    }
});

router.put('/:username/mcc', userCheck, async (req, res) => {
    try {
        const { _id } = req.user;
        const { username } = req.params;
        if ( username !== req.user.username) {
            return res.status(401).send({error: 'User not correct.'});
        }
        const cc = Date.now().toString(process.env.CC_NUM).toUpperCase();
        const user = await User.findByIdAndUpdate(_id, {
            cc
        }, { new: true });
        req.user = user;
        res.send(user);
    } catch (e) {
        res.status(404).send({ error: e });
    }
});

// router.put('/:username/pwd', userCheck, async (req, res) => {

// })

// router.post('/:username/forget', userCheck, async (req, res) => {

// })

router.put('/:username', userCheck, async (req, res) => {
    try {
        const { _id } = req.user;
        const { username } = req.params;
        if ( username !== req.user.username) {
            return res.status(401).send({error: 'User not correct.'});
        }
        const { name, fullName, profile, skills,
            works, simpleProfile } = req.body;
        const user = await User.findByIdAndUpdate(_id, {
            name, fullName, profile, skills,
            works, simpleProfile
        }, { new: true });
        res.send(user);
    } catch (e) {
        res.status(404).send({ error: e });
    }
});

module.exports = router;