const express = require('express');
const _ = require('lodash');

const User = require('../models/User');

const router = express.Router();

router.post('/check', async (req, res) => {
	try {
		const { token } = req.body;
		const user = await User.findByToken(token);
		const newToken = user.generateToken();
		res.cookie('token', newToken);
		res.send(_.assign(user.toJSON(), { token: newToken }));
	} catch (e) {
		res.status(404).send({ error: e });
	}
});

router.post('/login', async (req, res) => {
	try {
		const { username, password } = req.body;
		const user = await User.findByCredentials(username, password);
		const token = user.generateToken();
		res.cookie('token', token);
		res.header('auth', token).send(_.assign(user.toJSON(), { token }));
	} catch (e) {
		res.status(404).send({ error: e });
	}
});

router.get('/logout', (req, res) => {
	res.clearCookie('token');
	res.clearCookie('cc');
	res.removeHeader('auth');
	res.removeHeader('cc');
	res.send({ token: null, user: null });
});

// router.post('/add', async (req, res) => {
// 	const { username, password } = req.body;
// 	await new User({
// 		account, password
// 	}).save();
// 	res.send({});
// });

router.post('/cc', async (req, res) => {
	try {
		const { token } = req.cookies;
		let user = await User.findByToken({ token });
		if (user) {
			res.cookie('cc', user.cc);
			return res.header('cc', user.cc).send({cc: user.cc});
		}
	} catch (e) {}
	const { cc, username } = req.body;
	user = await User.findOne({ username, cc });
	if (!user) {
		return res.status(404).send('CC is error.');
	}
	if (!req.cookies.cc) {
		user.count += 1;
		await user.save();
	}
	res.cookie('cc', cc);
	res.header('cc', cc).send({cc});
});

module.exports = router;
