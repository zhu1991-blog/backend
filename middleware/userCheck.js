const User = require('../models/User');

module.exports = async (req, res, next) => {
    try {
        // const { token } = req.cookies;
        const token = req.header('auth');
        const user = await User.findByToken(token);
        if (!user) {
            throw new Error('You must login.');
        }
        req.user = user;
        next();
    } catch (e) {
        res.status(401).send({ error: e });
    }
}