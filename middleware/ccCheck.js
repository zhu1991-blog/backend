const User = require('../models/User');
const _ = require('lodash');

module.exports = async (req, res, next) => {
    try {

        // const { token } = req.cookies;
        const token = req.header('auth');
        let user = null;
        if (token && token !== 'null') {
            user = await User.findByToken(token);
            req.user = user;
        }
        if (!user) {
            const { userId, username } = req.params;
            // const { cc } = req.cookies;
            const cc = req.header('cc');
            user =  await User.findOne({ cc: cc, $or: [{_id: userId}, {username: username}] });
            if (!user) {
                 throw new Error('You have not a certification code.');
            }
        }

        next();
    } catch (e) {
        res.status(401).send({error: e});
    }
}