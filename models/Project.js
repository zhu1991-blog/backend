const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true,
    },
    startTime: Date,
    endTime: Date,
    mainLang: String,
    langs: String,
    tools: String,
    os: String,
    cDate: {
        type: Date,
        default: Date.now()
    },
    type: String,
    desc: String,
    link: String,
    _userId: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'User'
    }
});

module.exports = mongoose.model('Project', schema);