const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const _ = require('lodash');

const skill = require('./Skill');

const schema = new mongoose.Schema({
    name: String,
    fullName: String,
    profile: String,
    simpleProfile: String,
    skills: [skill.schema],
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    cc: {
        type: String,
        default: Date.now().toString(process.env.CC_NUM).toUpperCase()
    },
    profileImg: String,
    count: {
        type: Number,
        default: 0
    }
});

schema.pre('save', function(next) {
    let user = this;
    if (user.isModified('password')) {
        try {
            let { password } = user;
            const salt = bcrypt.genSaltSync(10);
            const hash = bcrypt.hashSync(password, salt);
            user.password = hash;
        } catch (e) {
            console.error(e);
        }
    }
    next();
});

schema.statics.findByCredentials = function(username, password) {
    let User = this;
    return User.findOne({username}).then(user => {
        if (!user) return Promise.reject();

        return new Promise((resolve, reject) => {
            bcrypt.compare(password, user.password, (err, success) => {
                if (success)
                    resolve(user);
                else 
                    reject();
            });
            
        });
    });
}

schema.methods.toJSON = function() {
    let user = this;
    let userObject = user.toObject();
    return _.pick(userObject, 
        [
            '_id', 
            'username',
            'name',
            'fullName',
            'profile',
            'skills',
            'cc',
            'simpleProfile',
            'count'
        ]
    );
}

schema.statics.findByToken = function (token) {
    let User = this;
    let decoded;
    try {
        decoded = jwt.verify(token, process.env.SECRET);
    } catch (e) {
        return Promise.reject();
    }

    return User.findOne({
        '_id': decoded._id,
        'username': decoded.username
    });
};

schema.methods.generateToken = function() {
    let user = this;
    let token = jwt.sign({
        _id: user._id.toHexString(), 
        username: user.username,
    }, process.env.SECRET).toString();
    return token;
};


module.exports = mongoose.model('User', schema);