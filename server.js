require('./config');

const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const session = require('express-session');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');

const auth = require('./routes/auth');
const project = require('./routes/apis/project');
const user = require('./routes/apis/user');

const app = express();
const PORT = process.env.PORT;

const isProduction = process.env.NODE_ENV === 'production';

mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI);

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(session({
	secret: process.env.SESSION_SECRET,
	cookie: {
		secure: true,
	},
	resave: false,
	saveUninitialized: true
}));
app.use(cookieParser());

if (isProduction) {
	app.all('*', (req, res, next) => {
		res.header('Access-Control-Allow-Origin', "https://zhu1991.github.io");
		res.header("Access-Control-Allow-Headers", "Content-Type, cc, auth");
		res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE");
		next();
	});
}

app.use('/auth', auth);
app.use('/api/project', project);
app.use('/api/user', user);

if (isProduction) {
	const path = require('path');
	app.use(express.static(path.resolve(__dirname, 'client', 'build')));
	app.get('*', (req, res) => {
		res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
	});
}

app.listen(PORT, () => console.log('server started!', PORT));

module.exports = app;
