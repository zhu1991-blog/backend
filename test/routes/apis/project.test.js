const request = require('supertest');
const expect = require('expect');
const { ObjectID } = require('mongodb');
const _ = require('lodash');

const app = require('../../../server');
const Project = require('../../../models/Project');
const { login } = require('./seed/seed');

describe('API Project', () => {
    const _id = new ObjectID().toHexString();
    const header = {};
    let user;

    before('Login', async () => {
        const res = await login();
        header.cookie = 'token=' + res.token;
        user = res.user;
    });

    let project;
    beforeEach(async () => {
        await Project.remove({ _id });
        const p = await new Project({
            _id,
            name: "Test Name",
            startTime: "2018/01/01",
            endTime: "2018/01/02",
            langs: "Test langs",
            tools: "Test tools",
            os: "Test OS",
            desc: "Test Desc",
            _userId: user._id
        }).save();
        project = JSON.parse(JSON.stringify(p));
    });

    afterEach(async () => {
        await Project.remove({ _id });
    });

    describe('GET', () => {

        it('should return 404', async () => {
            await request(app)
                .get('/api/project/123/123')
                .expect(404);
        });

        it('shuld return a Project and 200', async () => {
            const res = await request(app)
                .get(`/api/project/${user._id}/${_id}`)
                .expect(200);

            expect(typeof res.body).toBe('object');
            expect(res.body._id).toBe(_id);
        })

        it('should return an Array and 200', async () => {
            const res = await request(app)
                .get(`/api/project/${user._id}`)
                .expect(200);

            expect(res.body).toEqual(
                expect.arrayContaining([project])
            );
        });
    })

    describe('PUT', () => {
        it('should return 404', async () => {
            await request(app)
                .put(`/api/project/123/123`)
                .set(header)
                .expect(404);
        })

        it('should return a new Proejct and 200', async () => {
            const newProject = project;
            newProject.name = "Test new name";
            const res = await request(app)
                .put(`/api/project/${user._id}/${_id}`)
                .set(header)
                .send(newProject)
                .expect(200);

            expect(res.body.name).toBe(newProject.name);
        });
    });

    describe('DELETE', () => {

        it('should return 404', async () => {
            await request(app)
                .delete(`/api/project/123/123`)
                .set(header)
                .expect(404);
        });

        it('should return 404 too', async () => {
            await request(app)
                .delete(`/api/project/${user._id}`)
                .set(header)
                .expect(404);
        })

        it('should return 200 and a project', async () => {
            const res = await request(app)
                .delete(`/api/project/${user._id}/${_id}`)
                .set(header)
                .expect(200);

            expect(res.body).toMatchObject(project);

        })
    });

});