const request = require('supertest');
const expect = require('expect');
const _ = require('lodash');

const app = require('../../../server');
const User = require('../../../models/User');
const { login } = require('./seed/seed');

describe('API User', () => {

    let user;
    const header = {};

    before('Login', async () => {
        const res = await login();
        user = res.user;
        header.cookie = 'token=' + res.token;
    });

    after(async () => {
        await User.findByIdAndUpdate(user._id, user);
    });

    describe('GET', () => {
        describe('/api/user/:username', () => {

            it('should return 404', async () => {
                const res = await request(app).get('/api/user/123');
                expect(res.status).toBe(404);
            });

            it('should return a User and 200', async () => {
                const res = await request(app).get('/api/user/' + user.username).set(header);
                const expectUser = _.assign(user.toJSON(), {
                    _id: user._id.toString()
                });
                expect(res.status).toBe(200);
                expect(res.body).toEqual(expectUser);
            });
        });
    });


    describe('PUT', () => {
        describe('/api/user/:username', () => {

            it('should return 401', async () => {
                const res = await request(app).put('/api/user/' + user.username);
                expect(res.status).toBe(401);
            });
    
            it('should return a New User and 200', async () => {
                const newUser = _.assign(_.cloneDeep(user.toObject()), {
                    name: "new Username",
                    _id: user._id.toString()
                });
                const res = await request(app).put('/api/user/' + user.username).set(header).send(newUser);
                expect(res.status).toBe(200);
                expect(res.body._id).toBe(newUser._id);
                expect(res.body.name).not.toBe(user.name);
            });
        });

        describe('/api/user/:username/mcc', () => {
            it('should returna a 401', async () => {
                const res = await request(app).put(`/api/user/${user.username}/mcc`);
                expect(res.status).toBe(401);
            })

            it('should return a new "cc" and 200', async () => {
                const res = await request(app)
                    .put(`/api/user/${user.username}/mcc`)
                    .set(header);
                expect(res.status).toBe(200);
                expect(res.body.cc).not.toBe(user.cc);
            });
        })
    });
});
