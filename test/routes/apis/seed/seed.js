const User = require('../../../../models/User');

const login = async () => {
    const user = await User.findByCredentials(process.env.ACCOUNT, process.env.PASSWORD);
    const token = user.generateToken();
    return { user, token };
};

module.exports = {
    login
}